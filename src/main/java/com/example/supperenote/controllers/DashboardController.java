package com.example.supperenote.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class DashboardController {

    @GetMapping("/sign-in")
    public String showSignInPage() {
        return "views/auth/sign-in";
    }

    @GetMapping("/sign-up")
    public String showSignUpPage() {
        return "views/auth/sign-up";
    }

    @GetMapping("/forgot-password")
    public String showForgotPasswordPage() {
        return "views/auth/forgot-password";
    }

    @GetMapping("/change-password")
    public String showChangePasswordPage() {
        return "views/auth/change-password";
    }

    @GetMapping("/")
    public String showDashboardPage() {
        return "views/projects/dashboard";
    }

    @GetMapping("/create-project")
    public String showCreateProjectPage() {
        return "views/projects/create";
    }

    @GetMapping("/edit-project")
    public String showEditProjectPage() {
        return "views/projects/edit";
    }

    @GetMapping("/project-details")
    public String showProjectDetailsPage() {
        return "views/projects/details";
    }

    @GetMapping("/shared-with-me")
    public String showSharedWithMedPage() {
        return "views/shared-projects/shared-with-me";
    }

    @GetMapping("/new-share")
    public String showNewSharePage() {
        return "views/shared-projects/new-share";
    }

    @GetMapping("/edit-roles")
    public String showEditRolesPage() {
        return "views/shared-projects/edit-roles";
    }

    @GetMapping("/shared-project-details")
    public String showSharedProjectDetailsPage() {
        return "views/shared-projects/shared-project-details";
    }

    @GetMapping("/topics")
    public String showTopicsPage() {
        return "views/topics/index";
    }

    @GetMapping("/create-topic")
    public String showCreateTopicPage() {
        return "views/topics/create";
    }

    @GetMapping("/edit-topic")
    public String showEditTopicPage() {
        return "views/topics/edit";
    }

    @GetMapping("/topic-details")
    public String showTopicDetailsPage() {
        return "views/topics/details";
    }

    @GetMapping("/vocabularies")
    public String showVocabulariesPage() {
        return "views/vocabularies/index";
    }

    @GetMapping("/create-vocabulary")
    public String showCreateVocabularyPage() {
        return "views/vocabularies/create";
    }

    @GetMapping("/edit-vocabulary")
    public String showEditVocabularyPage() {
        return "views/vocabularies/edit";
    }

    @GetMapping("/vocabulary-details")
    public String showVocabularyDetailsPage() {
        return "views/vocabularies/details";
    }

    @GetMapping("/page-not-found")
    public String showPageNotFound() {
        return "views/errors/404";
    }

    @GetMapping("/forbidden")
    public String showForbiddenPage() {
        return "views/errors/403";
    }
}
