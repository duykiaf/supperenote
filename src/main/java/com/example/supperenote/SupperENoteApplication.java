package com.example.supperenote;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SupperENoteApplication {

    public static void main(String[] args) {
        SpringApplication.run(SupperENoteApplication.class, args);
    }

}
