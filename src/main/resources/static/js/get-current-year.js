$(document).ready(function(){
    const currentYear = new Date().getFullYear();
    $("#copyright").text("Copyright &copy; Duy Nguyen " + currentYear);
});
