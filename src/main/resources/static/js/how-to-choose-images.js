
$(document).ready(function(){
    $("input[name='imageRadioLabel']").on("change", function(){
        if ($("#imageUrlRadioLabel").is(":checked")) {
            $("#imageFromDevice").prop("disabled", true);
            $("#imageUrl").prop("disabled", false);
        } else {
            $("#imageUrl").prop("disabled", true);
            $("#imageFromDevice").prop("disabled", false);
        }
    });
});
